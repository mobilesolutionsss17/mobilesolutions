package android.campus02.at.quiz;


import java.util.ArrayList;
import java.util.Arrays;

class Question {

    private String text;
    private Answer correctAnswer;
    private ArrayList<String> answers = new ArrayList<>();

    enum Answer {
        ONE, // ordinal: 0
        TWO,
        THREE,
        FOUR
    }


    Question(String text,
             Answer correctAnswer,
             String answer1,
             String answer2,
             String answer3,
             String answer4) {
        this.text = text;
        this.correctAnswer = correctAnswer;
        this.answers.addAll(Arrays.asList(answer1, answer2, answer3, answer4));
    }

    boolean isCorrect(String answerText) {
        return answers.get(correctAnswer.ordinal()).equals(answerText);
    }

    String get(Answer answer) {
        return this.answers.get(answer.ordinal());
    }

    String getText() {
        return text;
    }
}
