package android.campus02.at.quiz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    // Log-Tag
    private static final String TAG = "MainActivity";
    // Keys zum Speichern des Activity-States (questionIndex, actualAnswer).
    public static final String KEY_QUESTION_INDEX = "KEY_QUESTION_INDEX";
    public static final String KEY_ACTUAL_ANSWER = "KEY_ACTUAL_ANSWER";

    // Interner State des Quiz-Ablaufs.
    private Question actualQuestion;
    private int questionIndex;
    private String actualAnswer;

    // Fixe Liste an Fragen im Speicher.
    private List<Question> questions = new ArrayList<>();
    {
        questions.add(new Question(
                "Ein zylindrisches Gefäß, in dem Speisen gekocht werden – oder kurz:",
                Question.Answer.THREE,
                "gu Lasch",
                "auf Lauf",
                "ein Topf",
                "ra Gout"));
        questions.add(new Question(
                "Wie heißt der Knabe, dem Wilhelm Tell den legendären Apfel vom Kopf schießt?",
                Question.Answer.TWO,
                "August",
                "Walter",
                "Heinrich",
                "Fritz"));
        questions.add(new Question(
                "Wie heißt der reiche Onkel von Donald Duck?",
                Question.Answer.FOUR,
                "Nepomuk",
                "Kasimir",
                "Richmond",
                "Dagobert"));
    }

    // Referenzen auf die UI-Elemente im Layout.
    private TextView question;
    private RadioButton answer1;
    private RadioButton answer2;
    private RadioButton answer3;
    private RadioButton answer4;
    private TextView feedback;
    private Button next;

    // Diese Methode wird vom Framework aufgerufen, wenn die App gestartet wird, zB. aber auch, wenn
    // das Display gedreht wird.
    // https://developer.android.com/reference/android/app/Activity.html
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate");

        // Zuweisen der anzuzeigenden Content View
        setContentView(R.layout.activity_main);

        // Holen der Referenzen auf Buttons, TextViews, ... im Layout
        question = (TextView) findViewById(R.id.question);
        answer1 = (RadioButton) findViewById(R.id.answer1);
        answer2 = (RadioButton) findViewById(R.id.answer2);
        answer3 = (RadioButton) findViewById(R.id.answer3);
        answer4 = (RadioButton) findViewById(R.id.answer4);
        feedback = (TextView) findViewById(R.id.feedback);
        next = (Button) findViewById(R.id.next);
        next.setOnClickListener(this);

        // Verdrahten der onclick-Listener, um auf Button-Klicks reagieren zu können.
        answer1.setOnClickListener(this);
        answer2.setOnClickListener(this);
        answer3.setOnClickListener(this);
        answer4.setOnClickListener(this);



        // Wiederherstellen des aktuellen Activity-Status (questionIndex, actualAnswer), falls
        // onCreate vom Framework aufgerufen wird.
        //
        // savedInstanceState ist null, wenn die App tatsächlich gestartet wird.
        // Falls das Bundle nicht null ist, sollten unsere in onSaveInstanceState gespeicherten
        // Attribute enthalten sein (zB. wenn das Display gedreht wird).
        String storedAnswer = null;
        if (savedInstanceState != null) {
            questionIndex = savedInstanceState.getInt(KEY_QUESTION_INDEX);
            storedAnswer = savedInstanceState.getString(KEY_ACTUAL_ANSWER);
        }

        // onCreate wird jedenfalls die aktuelle Frage angezeigt.
        // Bei einem tatsächlichen Start ist questionIndex = 0 und die erste Frage angezeigt.
        // Ansonsten ist questionIndex der Index, der in onSaveInstanceState gespeichert wird.
        displayQuestion();

        // Falls eine "storedAnswer" existiert, haben wir sie in onSaveInstanceState gespeichert,
        // weil beim Drehen des Displays bereits eine Antwort vom User gewählt war.
        // In dem Fall können wir den vorherigen Status der APP wiederherstellen, indem wir nach
        // Anzeige der aktuellen Frage die Button-States durch Aufruf von
        // verifyAnswer sicherstellen.
        //
        // Der vom User gewählte Radio-Button ist nach wie vor "checked", da wir diesen Zustand im
        // onCreate nicht verändern und der Status der UI-Elemente vom Framework selbst im Bundle
        // verwaltet wird.
        // Genau genommen müssen wir also nur jenen Status wiederherstellen, den wir im onCreate
        // verändern....
        if (storedAnswer != null) {
            verifyAnswer(storedAnswer);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    // Diese Methode wird vom Framework aufgerufen, um der Activity die Chance zu geben, sich
    // bestimmte Status-Attribute zu merken und im onCreate verwenden zu können, um den aktuellen
    // Status der APP wiederherstellen zu können (zB. falls sich das Display dreht).
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_QUESTION_INDEX, questionIndex);
        outState.putString(KEY_ACTUAL_ANSWER, actualAnswer);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }


    /**
     * Zeigt die aktuelle Frage an (und natürlich die entsprechenden Antworten) und kümmert sich
     * um das zugehörige Enablen/Disablen der entsprechenden Buttons.
     * Außerdem wird die actualAnswer = null gesetzt.
     */
    private void displayQuestion() {
        actualAnswer = null;
        next.setText(R.string.next_button);
        feedback.setText("");

        // Buttons restaurieren
        answer1.setEnabled(true);
        answer2.setEnabled(true);
        answer3.setEnabled(true);
        answer4.setEnabled(true);
        answer1.setChecked(false);
        answer2.setChecked(false);
        answer3.setChecked(false);
        answer4.setChecked(false);
        next.setEnabled(false);

        // Anzeigen der Frage (und Antworten)
        actualQuestion = questions.get(questionIndex);
        question.setText(actualQuestion.getText());
        answer1.setText(actualQuestion.get(Question.Answer.ONE));
        answer2.setText(actualQuestion.get(Question.Answer.TWO));
        answer3.setText(actualQuestion.get(Question.Answer.THREE));
        answer4.setText(actualQuestion.get(Question.Answer.FOUR));
    }

    /**
     * Überprüft, ob der angegebene answerText der korrekten Antwort auf die aktuelle Frage
     * entspricht.
     * @param answerText zu überprüfende Antwort
     */
    private void verifyAnswer(String answerText) {
        actualAnswer = answerText;

        // Buttons anpassen
        answer1.setEnabled(false);
        answer2.setEnabled(false);
        answer3.setEnabled(false);
        answer4.setEnabled(false);
        next.setEnabled(true);

        // Antwort überprüfen und Feedback geben.
        if(actualQuestion.isCorrect(answerText)) {
            feedback.setText(R.string.feedback_correct);
        } else {
            feedback.setText(R.string.feedback_wrong);
        }
    }

    /**
     * OnClickListener für alle Buttons auf der Seite - die Activity implementiert
     * View.OnClickListener selbst.
     *
     * Diese Methode wird immer aufgerufen, wenn auf einen der Buttons geklickt wird, die via
     *      setOnClickListener(this)
     * auf Klicks "hören".
     *
     * @param v die View Instanz, auf die der User geklickt hat (bei uns also ein RadioButton
     *          oder der next-Button).
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.answer1:
                verifyAnswer(answer1.getText().toString());
                Log.d(TAG, "Answer 1");
                break;
            case R.id.answer2:
                verifyAnswer(answer2.getText().toString());
                Log.d(TAG, "Answer 2");
                break;
            case R.id.answer3:
                verifyAnswer(answer3.getText().toString());
                Log.d(TAG, "Answer 3");
                break;
            case R.id.answer4:
                verifyAnswer(answer4.getText().toString());
                Log.d(TAG, "Answer 4");
                break;
            case R.id.next:
                questionIndex++;
                // Sicherheitsabfrage: falls wir bei der letzten Frage angelangt sind, zeigen wir
                // etwas anderes an und setzen den questionIndex so zurück, dass beim erneuten Klick
                // auf den next-Button wieder die erste Frage angezeigt wird.
                if (questionIndex >= questions.size()) {
                    Log.d(TAG, "Ende der Fragenliste erreicht.");
                    feedback.setText(R.string.feedback_game_over);
                    next.setText(R.string.next_button_tryagain);
                    questionIndex = -1;
                } else {
                    displayQuestion();
                }
                break;
            // Im default-case sollten wir nie landen, da sämtliche Views, die tatsächlich auf
            // Klicks reagieren, bereits oben behandelt sind. Nur zur Sicherheit loggen wir daher
            // diesen unerklärlichen Klick mit.
            default:
                Log.d(TAG, "Unknown button clicked: " + v.getId());
                break;
        }
    }
}
