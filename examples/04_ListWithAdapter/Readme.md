## Anzeige einer Liste von Messages mit eigenem Layout für Listeneinträge

### Aufgabenstellung

Aufbauend auf Beispiel 03 soll anstelle einer Liste von einfachen Strings eine Liste von selbstdefinierten Objekten mithilfe einer eigenen Ressource angezeigt werden. 

* Definieren unserer eigenen ``Message`` Klasse mit folgenden Properties
    * ``id``: String (zur späteren Verwendung)
    * ``subject``: String, Betreff der Nachricht
    * ``content``: String, Inhalt der Nachricht
    * ``sender``: String, Name des Absenders
* Erzeugen einer xml-Ressource für das Layout eines einzelnen Listeneintrags
    * Anlegen das Ressource-Files im Folder ``res/layout``: ``message_list_item.xml``
    * Das Layout soll einen ``ImageView`` links enthalten, rechts davon zwei ``TextView``s untereinander für die Anzeige von Subject und Sender. 
    * Am einfachsten dazu zwei geschachtelte ``LinearLayout``s verwenden
* Implementierung eines eigenen ListAdapters
    * Definieren der Klasse ``MessageListAdapter`` als private statische Klasse der Activity. Soll von ``ArrayListAdapter<Message>`` ableiten, damit wir nur die nötigsten Methoden überschreiben müssen.
    * Erzeugen eines Konstruktors, der als Parameter den Context bekommt und eine Liste/Array von Messages zur Anzeige (Im Super-Constructor-Aufruf des ``ArrayAdapter``s als Parameter für die Ressource-Id unser eigenes Layout angeben: ``R.layout.message_list_item``) 
    * Überschreiben der Methode ``getView``, um unseren eigenen View für einzelne Listeneinträge zu verwenden:
        * ``LayoutInflater``vom System verwenden, um aus der Ressource die View-Hierarchie zu erzeugen. 
        * Holen der entsprechenden Message aus dem übergebenen Message-Array
        * Zuweisen von Subject und Sender der richtigen Message an die TextViews ``R.id.subject_list_item``bzw. ``R.id.sender_list_item``
* Zuweisen des Adapters zum ``ListView`` (Methode ``setListAdapter``)