package android.campus02.at.listwithadapter.domain;

public class Message {

    private String id;
    private String subject;
    private String content;
    private String sender;

    public Message(String id, String subject, String content, String sender) {
        this.id = id;
        this.subject = subject;
        this.content = content;
        this.sender = sender;
    }

    public String getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public String getSender() {
        return sender;
    }
}
