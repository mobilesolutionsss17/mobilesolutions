package android.campus02.at.listwithadapter;

import android.campus02.at.listwithadapter.domain.Message;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Vorbereitete Liste von fixen Messages, sollten im
        // ListView vom Layout angezeigt werden.
        Message[] messages = new Message[] {
                new Message("id1", "Test", "Die allererste Nachricht überhaupt.", "System"),
                new Message("id117", "Eilmeldung", "Wer wird Zweiter?", "Hansi Hase"),
                new Message("id143", "Re: Irgendwas", "", "Gustav Gans"),
        };

        // Holen des ListView vom Layout
        ListView listView = (ListView) findViewById(R.id.listView);

        // Verknüpfen mit dem eigenen ListAdapter
        listView.setAdapter(new MessageListAdapter(this, messages));
    }

    /**
     * Einfache eigene ListAdapter-Implementierung, verwendet die Funktionalität des ArrayAdapter
     * (Ableitung) und überschreibt nur die nötigen Methoden.
     */
    private static class MessageListAdapter extends ArrayAdapter<Message> {

        // Liste der anzuzeigenden Messages, werden im Constructor übergeben
        private Message[] messages;

        /**
         * Constructor für unsere ListAdapter-Implementierung.
         * @param context der Context (die Activity, in welcher der Adapter verwendet wird)
         * @param objects die anzuzeigenden Objekte (unsere statischen Messages)
         */
        private MessageListAdapter(@NonNull Context context, @NonNull Message[] objects) {
            super(context, R.layout.message_list_item, objects);
            this.messages = objects;
        }


        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            // Um unseren eigenen View aus dem xml-File zu erzeugen, können wir den "LayoutInflater"
            // vom System verwenden (erzeugt die View-Hierarchie aus dem xml-File).
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // Falls ein "convertView" übergeben wird, können wir den wiederverwenden, ansonsten
            // müssen wir unseren eigenen View aus dem xml-File erzeugen.
            // Das "false" als letzter Parameter für inflate stellt sicher, dass der View NICHT fix
            // mit dem Container verdrahtet wird (in dem Fall müssten die IDs eindeutig sein, was
            // zu einer Fehlermeldung führen würde [da dann zB die ID "subject" mehrmals vorhanden
            // wäre]).
            View view = convertView == null
                    ? inflater.inflate(R.layout.message_list_item, parent, false)
                    : convertView;

            // Holen der anzuzeigenden Message für diesen View (diesen Listeneintrag).
            // Bei uns entspricht die "position" auch gleichzeitig dem Index in der internen Liste.
            Message message = messages[position];

            // Finden des Subjects und Befüllen mit dem Subject der Message
            TextView subject = (TextView) view.findViewById(R.id.subject_list_item);
            subject.setText(message.getSubject());

            // Finden des Sender und Befüllen mit dem Sender der Message
            TextView sender = (TextView) view.findViewById(R.id.sender_list_item);
            sender.setText(message.getSender());

            // Rückgabe der View
            return view;
        }
    }
}
