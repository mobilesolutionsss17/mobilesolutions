## Einstellungen speichern

### Aufgabenstellung 

Aufbauend auf Beispiel 06.

Das Beispiel soll um persistente Einstellungen erweitert werden. Der bisher als fixer String verwendete Benutzername zum Abrufen der Nachrichten vom Server soll über diese Einstellungen konfigurierbar sein. 


### Schritt 1: Verwenden des ``Menu``

* Definition eines Menüs in einem eigenen ResourceFile: ``menu/menu.xml``
* Dort Einrichten eines einzelnen Menü-Eintrags mit dem Label **Settings**.
* In der ``ListActivity`` die Methode ``onCreateOptionsMenu`` überschreiben und dort mithilfe des ``MenuInflater`` die neu erstellte Menü-Ressource laden. 

### Schritt 2: ``Preferences``

* Anlegen eines Preferences Resource Files unter ``xml/preferences.xml``.
* Darin Definition einer ``EditTextPreference`` für das Festlegen des verwendeten Benutzernamens (key=``preference_username``).
* Erzeugen einer ``SettingsActivity`` (ohne Layout-File).
* Erzeugen eines ``SettingsFragment extends PreferencesFragment``. Dort in der Methode ``onCreate`` Anzeige der Preferences mithilfe von ``addPreferencesFromResource``. 
* In ``SettingsActivity.onCreate`` Ersetzen der Default-Resource-Id ``android.R.id.content`` durch ein ``SettingsFragment`` (mithilfe des ``FragmentManager``).
* In der ``ListActivity`` die Methode ``onOptionsItemSelected`` überschreiben und bei Klick auf den "Settings"-Menü-Eintrag über einen ``Intent`` die ``SettingsActivity`` anzeigen.
*  Für das REST-API als Benutzername den Wert dieses neuen Settings verwenden, falls vorhanden. 
* Falls der Username in den Preferences noch nicht eingetragen wurde, soll die ``ListActivity`` direkt die Settings öffnen (auch wenn ein Leerstring als Username eingetragen wurde).