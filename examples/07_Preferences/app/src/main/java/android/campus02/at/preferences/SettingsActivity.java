package android.campus02.at.preferences;

import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // So würde es auch ohne SettingsFragment funktionieren, aber nachdem
        // die Methode "deprecated" ist, verwenden wir hier auch ein PreferenceFragment.
        //
        // Achtung: falls ohne Fragment verwendet, müsste die Activity
        //          extends PreferenceActivity
        //          verwenden.
        //
        //addPreferencesFromResource(R.xml.preferences);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }


    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences);
        }
    }

}
