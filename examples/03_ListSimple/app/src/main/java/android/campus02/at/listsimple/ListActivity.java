package android.campus02.at.listsimple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Vorbereitete Liste von fixen Messages, sollten im
        // ListView vom Layout angezeigt werden.
        String[] messages = new String[] {
                "Message 1",
                "Message 2",
                "Message 3",
                "Message 4",
                "Message 5"
        };

        // Holen des ListView vom Layout
        ListView listView = (ListView) findViewById(R.id.listView);

        // Verknüpfen mit dem ListAdapter
        listView.setAdapter(new ArrayAdapter<>(
                // Context (die Activity)
                this,
                // Layout für eine Zeile der Liste, Android bringt ein
                // paar Varianten mit.
                android.R.layout.simple_list_item_1,
                // Array der anzuzeigenden Items
                messages
        ));














    }
}
