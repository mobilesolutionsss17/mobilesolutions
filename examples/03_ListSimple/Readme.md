## Anzeige einer einfachen Liste von Strings

### Aufgabenstellung

Beginnend mit einer leeren Activity soll eine ``ListView`` in das Layout eingebaut werden und eine vorgegebene Liste von einfachen Strings anzeigen. 

* Entfernen des bestehenden TextView aus der leeren Activity 
* Einfügen eines ``ListView`` in die Activity (inklusive ID), Festlegen der richtigen Constraints im Layout.
* Definieren eines statischen Arrays von Strings (Listeneinträge)
* Anzeige der statischen Strings als Liste im ``onCreate`` der Activity
    * Finden und Holen der ``ListView`` aus dem Layout
    * Initialisieren eines ``ArrayAdapter<String>`` mit der Activity als Context, der Ressource ``android.R.layout.simple_list_item_1`` (aus dem SDK) und der Liste der anzuzeigenden Strings
    * Zuweisen des Adapters zum ``ListView`` (Methode ``setListAdapter``)