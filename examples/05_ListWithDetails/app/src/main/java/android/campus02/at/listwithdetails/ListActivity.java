package android.campus02.at.listwithdetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class ListActivity extends AppCompatActivity implements MessageSelectionReaction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Holen des ListView vom Layout
        ListView listView = (ListView) findViewById(R.id.listView);

        // Verknüpfen mit dem eigenen ListAdapter
        listView.setAdapter(new MessageListAdapter(this, Message.DUMMY_MESSAGES, this));
    }

    // "Callback", den wir in dem im MessageListAdapter definierten OnClickListener
    // aufrufen, um unserer ListActivity Bescheid zu sagen, dass eine Message
    // angeklickt wurde.
    public void onMessageClicked(int index) {
        // Wir wollen die Messageactivity öffnen, dazu definiert man einen Intent.
        Intent intent = new Intent(this, MessageActivity.class);
        // Zusätzlich merken wir uns den Index der Message beim Intent.
        intent.putExtra(MessageActivity.EXTRA_MESSAGE_INDEX, index);
        startActivity(intent);
    }

}
