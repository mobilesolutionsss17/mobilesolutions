package android.campus02.at.listwithdetails;

public class Message {

    private String id;
    private String subject;
    private String content;
    private String sender;

    // Vorbereitete Liste von fixen Messages, sollten im
    // ListView vom Layout angezeigt werden.
    public static  Message[] DUMMY_MESSAGES = new Message[] {
            new Message("id1", "Test", "Die allererste Nachricht überhaupt.", "System"),
            new Message("id117", "Eilmeldung", "Wer wird Zweiter?", "Hansi Hase"),
            new Message("id143", "Re: Irgendwas", "", "Gustav Gans"),
    };


    public Message(String id, String subject, String content, String sender) {
        this.id = id;
        this.subject = subject;
        this.content = content;
        this.sender = sender;
    }

    public String getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public String getSender() {
        return sender;
    }
}
