## Anzeige einer ausgewählten Nachricht in einer eigenen Activity

### Aufgabenstellung

Aufbauend auf Beispiel 04. 
Wenn der Anwender auf einen Eintrag in der angezeigten Liste klickt, soll die ausgewählte Nachricht in einer eigenen Activity angezeigt werden.

* Zuweisen eines ``OnClickListener``s für einen einzelnen Listeneintrag.
* Im ersten Schritt Ausgabe des Index der geklickten Nachricht mittels ``Toast.makeText``
* Anlegen einer neuen ``MessageActivity`` mit einem Layout für die Anzeige der Details der Message (jeweils mit Label): 
    * Subject
    * Content
    * Sender
* Ändern des OnClick-Verhaltens: bei Klick wird ``MessageActivity`` geöffnet:
    * Verwenden von ``startActivity(Intent)``
    * Aktivieren des "Zurück"-Pfeils in der ActionBar.
* Anzeige der richtigen Message in ``MessageActivity``
    * Liste der Messages in eine eigene Klasse verschieben, damit alle Activities darauf zugreifen können. 
    * ``Intent`` erweitern um ein Extra, das den Index der anzuzeigenden Message speichert.
    * Verwenden dieses Extras in ``MessageActivity``, um die Textfelder mit den Attributen der Message zu befüllen.
    

    
