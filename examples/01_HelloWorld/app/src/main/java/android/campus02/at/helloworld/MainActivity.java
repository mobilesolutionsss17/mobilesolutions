package android.campus02.at.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
    implements View.OnClickListener {


    private TextView label;

    //
    // Variante 1: Eigene Klasse, die den View.OnClickListener implementiert
    //
    // ist nur mehr zu Demonstrationszwecken in der Klasse, wird wie folgt verwendet:
    //
    //     button.setOnClickListener(new MyOnClickListener());
    //
    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            label.setText(R.string.label_replacement);
        }
    }

    //
    // Variante 2: Die Activity selbst implementiert den View.OnClickListener
    //
    // In dem Fall kann derselbe onClick-Listener für alle Buttons verwendet werden und man
    // unterscheidet über die ID des geklickten Buttons (View-Parameter), welche Aktion
    // tatsächlich auszuführen ist.
    //
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                label.setText(R.string.label_replacement2);
                break;
            case R.id.button2:
                label.setText(R.string.label_replacement);
                break;
            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        label = (TextView) findViewById(R.id.label);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        //
        // Variante 3: Anonyme innere Klasse, die den View.OnClickListener implementiert
        //
        // ist nur mehr zu Demonstrationszwecken in der Klasse, wird wie folgt verwendet:
        //
        //             button.setOnClickListener(new View.OnClickListener() {
        //                 @Override
        //                 public void onClick(View v) {
        //                     label.setText(R.string.label_replacement);
        //                 }
        //             });
    }
}
