package android.campus02.at.accessrestservice;

import android.campus02.at.accessrestservice.rest.MessageAPI;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity implements MessageSelectionReaction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Holen des ListView vom Layout
        ListView listView = (ListView) findViewById(R.id.listView);

        final MessageListAdapter adapter = new MessageListAdapter(
                this,
                MessageStore.getInstance().getMessages(),
                this);

        // Verknüpfen mit dem eigenen ListAdapter
        listView.setAdapter(adapter);

        // REST API Call erzeugen
        Call<List<Message>> call = MessageAPI.getApi().getIncomingMessages("mmair");

        // REST API Call aufrufen
        call.enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                Toast.makeText(ListActivity.this, "Success", Toast.LENGTH_SHORT).show();
                MessageStore.getInstance().setMessages(response.body());
                adapter.setMessages(response.body());
            }

            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
                Toast.makeText(ListActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });

    }

    // "Callback", den wir in dem im MessageListAdapter definierten OnClickListener
    // aufrufen, um unserer ListActivity Bescheid zu sagen, dass eine Message
    // angeklickt wurde.
    public void onMessageClicked(int index) {
        // Wir wollen die Messageactivity öffnen, dazu definiert man einen Intent.
        Intent intent = new Intent(this, MessageActivity.class);
        // Zusätzlich merken wir uns den Index der Message beim Intent.
        intent.putExtra(MessageActivity.EXTRA_MESSAGE_INDEX, index);
        startActivity(intent);
    }

}
