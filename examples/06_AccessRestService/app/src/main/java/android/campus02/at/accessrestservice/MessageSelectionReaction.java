package android.campus02.at.accessrestservice;

/**
 * Eigenes Interface, um auf die Auswahl (das Anklicken) einer Message reagieren zu
 * können, ohne dass der Adapter eine direkte Abhängigkeit auf die ListActivity hat.
 * Dadurch könnten verschiedene Activities den Adapter verwenden, indem sie das Interface
 * implementieren.
 */
interface MessageSelectionReaction {
    void onMessageClicked(int index);
}
