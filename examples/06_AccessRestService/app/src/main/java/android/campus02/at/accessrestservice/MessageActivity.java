package android.campus02.at.accessrestservice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.List;

public class MessageActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE_INDEX = "message_index";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        // Holen des Index der anzuzeigenden Message aus dem Intent
        // 0 als 2. Paramter bedeutet, dass 0 als Index verwendet wird,
        // wenn in den Extras kein Index gefunden wird.
        int index = getIntent().getIntExtra(EXTRA_MESSAGE_INDEX, 0);

        List<Message> messages = MessageStore.getInstance().getMessages();

        // Nur zur Sicherheit, falls aus irgendeinem Grund der Index aus den
        // Intent-Extras größer wäre als Nachrichten zur Verfügung stehen.
        // Damit dann keine ArrayIndexOutOfBoundsException kommt, verwenden wir
        // dann einfach ebenfalls 0 als Index.
        if (index >= messages.size()) {
            index = 0;
        }

        // Holen der entsprechenden anzuzeigenden Message.
        Message message = messages.get(index);

        // Holen des Subject-TextView, Befüllen mit dem Subject der Message
        TextView subject = (TextView) findViewById(R.id.detail_subject);
        subject.setText(message.getSubject());

        // Holen des Content-TextView, Befüllen mit dem Content der Message
        TextView content = (TextView) findViewById(R.id.detail_content);
        content.setText(message.getContent());

        // Holen des Sender-TextView, Befüllen mit dem Sender der Message
        TextView sender = (TextView) findViewById(R.id.detail_sender);
        sender.setText(message.getSender());
    }
}
