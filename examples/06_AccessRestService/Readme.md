## Laden von Nachrichten über ein REST API

### Aufgabenstellung

Aufbauend auf Beispiel 05. 

Anstelle der bisher verwendeten statischen Liste von Nachrichten sollen Messages von einem im Internet verfügbaren REST API (im JSON Format) geladen und angezeigt werden.

Dazu steht unter [http://messages.moarsoft.com](http://messages.moarsoft.com) eine entsprechende Schnittstellenbeschreibung zur Verfügung. 
Das beschriebene REST API ist unter [http://messages.moarsoft.com/rest](http://messages.moarsoft.com/rest) installiert (in der API-Beschreibung gibt es Links auf die tatsächliche Schnittstelle). 

Als HTTP-Client für den Zugriff auf die Schnittstelle verwenden wir die Library [Retrofit](http://square.github.io/retrofit/), für die Umwandlung von JSON zu java-Objekten die Library [Gson](https://github.com/google/gson).


#### Schritt 1: Einrichten der Libraries und Permissions

* Eintragen der Dependency auf Retrofit bzw. den entsprechenden Gson-Converter in ``build.gradle``.
* Internet-Zugriffsrechte für die Applikation einrichten. Dazu in ``AndroidManifest.xml`` die Permission ``android.permission.INTERNET``eintragen.


#### Schritt 2: Retrofit einrichten für den Zugriff auf das REST API

* Erzeugen des Interfaces ``MessageService`` mit einer einzelnen Methode 
    * ``Call<List<Message>> incoming(String username);``
* Annotieren des Interfaces für den HTTP-Zugriff (Retrofit-Annotationen)
* Erzeugen einer Klasse ``MessageAPI``, die eine Factory-Methode für die Erzeugung einer Implementierung von ``MessageService`` über Retrofit zur Verfügng stellt. 

#### Schritt 3: Durchführen eines REST API Zugriffs

* In ``ListActivity.onCreate`` das ``MessageService`` erzeugen und über die Methode ``incoming`` den Call erzeugen. Dabei als Parameter "mmair" für den Usernamen verwenden, da für diesen Benutzer bereits Nachrichten im System existieren. 
* Asynchroner Zugriff auf das REST API über ``Call.enqueue``, als Callback dabei eine anonyme innere Klasse verwenden. 
* Ausgabe, ob der Zugriff erfolgreich war oder nicht über einen entsprechenden ``Toast``. 

#### Schritt 4: Kontrolle der empfangenen Daten im Debugger

* Breakpoint setzen in der Methode ``onResponse`` des implementierten Callbacks. 
* Kontrolle über den Debugger, ob die selben Nachrichten ankommen (als java-Objekte) wie in der JSON-Repräsentation bei Zugriff über den Browser.

#### Schritt 5: Einführen eines ``MessageStore``

* Damit die geladenen Messages auch von ``MessageActivity`` aus zugreifbar sind.
* Erzeugen der (statischen) Klasse ``MessageStore`` mit folgender Funktionalität:
    * Verwalten einer Liste von ``Message`` Objekten (``getMessages``, ``setMessages``)
    * Factory Methode für die Erzeugung einer ``MessageStore`` Singleton Instanz. 
    * Dazu privater Konstruktor, damit von außen nicht erzeugbar. 
* Befüllen des ``MessageStore`` nach dem erfolgreichen Laden der Messages vom Server. 
* Laden der Messages im ``onCreate`` nur, falls keine Nachrichten im Store vorhanden sind. 
* Verwenden des ``MessageStore`` in der ``MessageActivity`` 
* Beim Klick auf einen Eintrag in der Liste sollte dann die ``MessageActivity`` bereits die richtigen Daten vom Server anzeigen. 

#### Schritt 6: Update der Liste nach erfolgreichem Laden der Messages

* Ändern des ``MessageListAdapter`` von Verwendung eines Arrays auf die Verwendung einer Liste (mit ``Array`` wird eine nachträglichen Änderung der Nachrichten vom ``ListView`` nicht erkannt).
* Optional: das interne Merken der Nachrichten im Adapter wäre gar nicht nötig - umarbeiten auf die internen Methoden zum Holen einer bestimmten Nachricht aus dem Adapter, Löschen der Property ``messages``. 
* Nach erfolgreichem Holen der Nachrichten vom Server (``Callback.onResponse``) Setzen der geholten Nachrichten auf dem ``MessageListAdapter``. 


* Optional: Umarbeiten der ``MessageSelectionReaction`` auf einen Observer (``addObserver``, Callback-Methode ``onMessageSelected(int position)``.

 