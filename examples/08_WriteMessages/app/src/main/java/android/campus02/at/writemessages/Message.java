package android.campus02.at.writemessages;

import com.google.gson.annotations.SerializedName;

public class Message {

    private String id;
    private String subject;
    private String content;
    @SerializedName("sender_display")
    private String sender;
    @SerializedName("sender_id")
    private String usernameSender;
    @SerializedName("sender_image_url")
    private String senderImageUrl;

    public Message(String senderUserName, String subject, String content) {
        this.usernameSender = senderUserName;
        this.subject = subject;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public String getSender() {
        return sender;
    }

    public String getUsernameSender() {
        return usernameSender;
    }

    public String getSenderImageUrl() {
        return senderImageUrl;
    }
}
