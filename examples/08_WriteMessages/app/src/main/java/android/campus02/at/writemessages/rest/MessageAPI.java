package android.campus02.at.writemessages.rest;


import android.campus02.at.writemessages.Message;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Kapselt den Zugriff auf das REST API und die Erzeugung der Retrofit-Implementierung des
 * Service-Interfaces.
 */
public class MessageAPI {

    // Singleton Instanz (für Factory Methode)
    private static MessageService service;

    // Basis-URL (Endpoint) des REST API
    private static final String BASE_URL = "http://messages.moarsoft.com";

    /**
     * Abbildung des REST APIs als Interface im Code.
     * Über dieses Interface können wir in unserem Code auf das API zugreifen.
     */
    public interface MessageService {

        /**
         * Holt die Liste der empfangenen Nachrichten für einen bestimmten Benutzer über
         * REST (JSON) vom Server.
         * @param username der Username des Benutzers, für den die Nachrichten abgerufen
         *                 werden sollen.
         * @return Das "Call"-Objekt für den (synchronen oder asynchronen) Aufruf des API-Calls.
         *         Der Typ-Parameter (List<Message>) gibt den Rückgabewert bei erfolgreichem
         *         Aufruf der API-Methode an. Dieser Typ muss zur JSON-Struktur der Server-Antwort
         *         passen, damit in unserem Fall aus den JSON-Objekten via Gson Message-Objekte
         *         erzeugt werden können.
         */
        // Retrofit-Annotation, beschreibt die verwendete HTTP-Methode und den Pfad zum
        // entsprechenden Service.
        @GET("/rest/messages/{username}/incoming")
                                               // Retrofit Annotation, mapped den Parameter auf
                                               // den Platzhalter in der URL
        Call<List<Message>> getIncomingMessages(@Path("username") String username);

        @POST("/rest/messages")
        Call<Message> send(@Body Message message);
    }

    /**
     * Factory Methode für die Erzeugung der Retrofit-Implementierung des MessageService.
     * @return Service-Implementierung
     */
    public static MessageService getApi() {
        // beim ersten Aufruf: Erzeugen des Singletons.
        if (service == null) {

            // Konfigurieren von Retrofit für den Zugriff auf das REST API (Builder Pattern).
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL) // Service-Endpoint
                    .addConverterFactory(GsonConverterFactory.create()) // Gson verwenden
                    .build();

            // Service Implementierung erzeugen lassen
            service = retrofit.create(MessageService.class);
        }
        return service;
    }

}

