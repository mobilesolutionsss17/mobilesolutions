package android.campus02.at.writemessages;

import android.campus02.at.writemessages.rest.MessageAPI;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity implements MessageSelectionReaction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = preferences.getString("preference_username", null);

        // Falls kein Username in den Preferences gespeichert ist, leiten wir
        // direkt auf die Settings um und zeigen nicht die Liste der Nachrichten an.
        if (username == null || username.isEmpty()) {
            openSettings();
            return;
        }


        // Holen des ListView vom Layout
        ListView listView = (ListView) findViewById(R.id.listView);

        final MessageListAdapter adapter = new MessageListAdapter(
                this,
                MessageStore.getInstance().getMessages(),
                this);

        // Verknüpfen mit dem eigenen ListAdapter
        listView.setAdapter(adapter);

        // floating action button
        FloatingActionButton actionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListActivity.this, CreateMessageActivity.class);
                startActivity(intent);
            }
        });

        // REST API Call erzeugen
        Call<List<Message>> call = MessageAPI.getApi().getIncomingMessages(username);

        // REST API Call aufrufen
        call.enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                if (response.code() == 200) {
                    Toast.makeText(ListActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    MessageStore.getInstance().setMessages(response.body());
                    adapter.setMessages(response.body());
                } else {
                    Toast.makeText(ListActivity.this, "Error: " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
                Toast.makeText(ListActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });

    }

    // "Callback", den wir in dem im MessageListAdapter definierten OnClickListener
    // aufrufen, um unserer ListActivity Bescheid zu sagen, dass eine Message
    // angeklickt wurde.
    public void onMessageClicked(int index) {
        // Wir wollen die Messageactivity öffnen, dazu definiert man einen Intent.
        Intent intent = new Intent(this, MessageActivity.class);
        // Zusätzlich merken wir uns den Index der Message beim Intent.
        intent.putExtra(MessageActivity.EXTRA_MESSAGE_INDEX, index);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    private void openSettings() {

        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                openSettings();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
