package android.campus02.at.writemessages;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Einfache eigene ListAdapter-Implementierung, verwendet die Funktionalität des ArrayAdapter
 * (Ableitung) und überschreibt nur die nötigen Methoden.
 */
class MessageListAdapter extends ArrayAdapter<Message> {

    // Callback, wird notifiziert, wenn der Benutzer auf eine Message klickt.
    private MessageSelectionReaction reaction;

    /**
     * Constructor für unsere ListAdapter-Implementierung.
     * @param context der Context (die Activity, in welcher der Adapter verwendet wird).
     *                Wir verwenden wir hier direkt ListActivity als Typ, damit wir auf
     *                zB "onMessageClicked" zugreifen können. Activity leitet von
     *                Context ab, kann also dem Super-Constructor einfach als Context
     *                übergeben werden.
     * @param objects die anzuzeigenden Objekte (unsere statischen Messages)
     */
    MessageListAdapter(@NonNull Context context, @NonNull List<Message> objects,
                       MessageSelectionReaction reaction) {
        super(context, R.layout.message_list_item, objects);
        this.reaction = reaction;
    }

    public void setMessages(List<Message> messages) {
        clear();
        addAll(messages);
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Um unseren eigenen View aus dem xml-File zu erzeugen, können wir den "LayoutInflater"
        // vom System verwenden (erzeugt die View-Hierarchie aus dem xml-File).
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Falls ein "convertView" übergeben wird, können wir den wiederverwenden, ansonsten
        // müssen wir unseren eigenen View aus dem xml-File erzeugen.
        // Das "false" als letzter Parameter für inflate stellt sicher, dass der View NICHT fix
        // mit dem Container verdrahtet wird (in dem Fall müssten die IDs eindeutig sein, was
        // zu einer Fehlermeldung führen würde [da dann zB die ID "subject" mehrmals vorhanden
        // wäre]).
        View view = convertView == null
                ? inflater.inflate(R.layout.message_list_item, parent, false)
                : convertView;

        // Zuweisen des OnClickListeners: damit reagieren wir darauf, wenn der
        // Anwender auf einen Eintrag in der Liste klickt.
        // - wir setzen ihn direkt auf die Root-View eines Listeneintrags
        //   (R.layout.message_list_item), damit wirklich jeder Klick auf den
        //   Listeneintrag registriert wird (alternativ könnte man ihn auch auf die
        //   einzelnen TextViews bzw. ImageViews setzen).
        // - und wir verwenden in dem Fall eine anynome innere Klasse, weil wir
        //   dadurch im Scope der getView-Methode bleiben und direkt auf den
        //   Parameter "position" zugreifen können.
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getContext(), "" + position, Toast.LENGTH_SHORT).show();
                reaction.onMessageClicked(position);
            }
        });

        // Holen der anzuzeigenden Message für diesen View (diesen Listeneintrag).
        // Bei uns entspricht die "position" auch gleichzeitig dem Index in der internen Liste.
        Message message = getItem(position);

        // Finden des Subjects und Befüllen mit dem Subject der Message
        TextView subject = (TextView) view.findViewById(R.id.subject_list_item);
        subject.setText(message.getSubject());

        // Finden des Sender und Befüllen mit dem Sender der Message
        TextView sender = (TextView) view.findViewById(R.id.sender_list_item);
        sender.setText(message.getSender());

        // Laden des Image falls vorhanden
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        Picasso.with(getContext())
                .load(message.getSenderImageUrl())
                .error(R.mipmap.ic_launcher)
                .into(imageView);

        // Rückgabe der View
        return view;
    }
}
