## Versenden von Nachrichten

### Aufgabenstellung

Aufbauend auf Beispiel 07.

Es sollen Nachrichten nun auch verschickt werden können.
Dazu soll ein eigener ``FloatingActionButton`` auf der Liste der angezeigten Nachrichten verwendet werden, um eine ``CreateMessageActivity`` zu öffnen. 

#### Schritt 1: Einfügen des "New Message" Buttons

* Ein ``FloatingActionButton`` wird in das ``activity_list.xml`` RessourceFile dazugegeben. 
* Positionieren Sie den Button im rechten unteren Bereich der Liste (direkt auf der Liste - "floating"). 
* Wählen Sie ein passendes Icon für den Button (Plus?).

#### Schritt 2: Erzeugen einer Activity zum Erstellen einer Nachricht

* Erzeugen Sie eine neue Activity ``CreateMessageActivity`` inklusive Layout. 
* Das Layout sollte eine Betreffzeile, einen Bereich für den Nachrichteninhalt, sowie einen ``Abbrechen`` und ``Senden`` Button beinhalten. 
* Öffnen Sie diese Activity beim Klick auf den ``FloatingActionButton``. 

#### Schritt 3: Erweitern des REST API Clients

* Erweitern Sie das ``MessageService`` um eine neue Methode zum Versenden einer Message. 
    * Die Methode wird mittels ``POST`` aufgerufen, die (relative) URL lautet "/rest/messages".
    * Die Methode hat einen Parameter vom Typ ``Message``, der mit ``@Body`` annotiert wird.
    * Das REST API sendet bei Erfolg die neu erstellte Message wieder zurück (inklusive neuer Felder wie ``id``). 
* Verwenden Sie diese neue API-Methode beim Klick auf ``Senden``. Beenden Sie nach erfolgreichem Senden die Activity mittels ``finish()`` (dann wird wieder die ListActivity angezeigt). 
* Bei Klick auf ``Abbrechen`` beenden Sie einfach die Activity. 


