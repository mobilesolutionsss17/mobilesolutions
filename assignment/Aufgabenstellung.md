## Aufgabenstellung

Erweitern Sie die "Messages" App, ausgehend vom mitgelieferten Code-Stand. 

### Teil 1: Liste aller Benutzer

Es soll eine neue Activity erstellt werden, welche eine Liste aller dem REST API bekannten Benutzer anzeigt. Im Menü soll es einen neuen Eintrag "Benutzer" geben. Ein Klick darauf öfffnet die neue Activity.

#### Definieren der Klasse ``User``

* Bestimmen Sie die möglichen Properties anhand der Beschreibung des REST APIs unter [http://messages.moarsoft.com](http://messages.moarsoft.com)
* Korrigieren Sie etwaige Abweichungen in den Property-Namen mithilfe der ``@SerializedName`` Annotation.

#### Definieren der Klasse ``UserStore``

* Dient zum Verwalten einer Liste von Benutzern (wie ``MessageStore`` für Messages).
* Implementieren Sie eine Factory-Methode für das Holen der Singleton-Instanz des ``UserStore``. 


#### Definieren der Klasse ``UserAPI``

* Analog zu ``MessageAPI``
* Definieren Sie das Interface ``UserService``
    * Enthält nur eine Methode ``getAllUsers``.
    * Die Signatur und die verwendete Annotation für die Methode soll der Beschreibung des REST API Calls [http://messages.moarsoft.com/users.html#liste-aller-benutzer](http://messages.moarsoft.com/users.html#liste-aller-benutzer) entsprechen. 
* Implementieren Sie auch hier eine Factory-Methode für den Zugriff auf die mit ``Retrofit`` erstellte Singleton-Implementierung des ``UserService``-Interfaces. 


#### ``UserListActivity`` erstellen

* Erzeugen Sie eine neue Activity ``UserListActivity`` inklusive Layout.
* Das Layout soll lediglich einen ``ListView`` enthalten, der den gesamten verfügbaren Platz ausfüllen soll.
* In der Methode ``onCreate`` soll das neue ``UserAPI`` verwendet werden, um vom Server die Liste aller Benutzer zu holen und den ``UserStore`` zu befüllen. 

#### Adapter für die Liste der Benutzer

* Implementieren Sie eine Klasse ``UserListAdapter extends ArrayAdapter<User>``. *Achtung*: Sie brauchen keine ``SelectionReaction`` vorsehen, da diese Liste etwas anders reagiert als die Liste der Nachrichten.
* Dieser Adapter arbeitet mit einer Liste von Benutzern, die sowohl über den Konstruktor, als auch über die Methode ``setUsers(List<User> users)`` zugewiesen werden können. 
* Für die Anzeige eines einzelnen Listeneintrags verwenden Sie die bereits bestehende Ressource ``message_list_item``). Der obere TextView  soll Vorname und Nachname des Senders anzeigen, der untere TextView seinen Benutzernamen.
* **Optional**: Der ImageView soll das Foto des Senders der Nachricht anzeigen (sofern vorhanden). Zum Laden und Anzeigen des Fotos wird die Library "Picasso" verwendet. 
* Verwenden Sie diesen neuen Adapter in der ``UserListActivity`` für den darin enthaltenen ``ListView``.
* **Optional**: ``message_list_item.xml`` sowie die dort verwendeten IDs passen nicht mehr wirklich gut, da damit jetzt ja auch Informationen über einen Benutzer angezeigt werden. Ändern Sie daher den Namen ``message_list_item.xml`` zu ``list_item.xml``, die Id ``R.id.subject_list_item`` zu ``R.id.list_item_line1`` und die Id ``R.id.sender_list_item``zu ``R.id.list_item_line2``.

#### Öffnen der Liste der Benutzer

* Erweitern Sie das Menü um einen neuen Eintrag "Benutzer". 
* Ein Klick auf diesen Menü-Eintrag öffnet die ``UserListActivity``. 


### Teil 2: Senden von "privaten" Nachrichten

Ausgehend von der Liste der Benutzer wird die App um die Möglichkeit erweitert, eine Nachricht an nur einen bestimmten einzelnen Benutzer zu versenden.
Dazu soll ein Klick auf einen Benutzer in der Liste diesen auswählen (Listenzeile bekommt eine andere Hintergrundfarbe). Zusätzlich gibt es einen ``FloatingActionButton`` - klickt man auf den, kann man eine Nachricht an den aktuell in der Liste ausgewählten Benutzer versenden. 

#### Markieren eines Benutzers bei Klick auf Listeneintrag

* Verwenden Sie die Methode ``ListView.setOnItemClickListener``, um auf Klicks auf einen Listeneintrag reagieren zu können. Der Listener ist dabei vom Typ ``AdapterView.OnItemClickListener``. 
* In der Methode ``onItemClick`` des Listeners merken Sie sich den Index (oder Username) des Benutzers, der angeklickt wurde. Zusätzlich soll die ``Selection`` auf dem ListView entsprechend gesetzt werden (Index).
* Setzen Sie ``AbsListView.CHOICE_MODE_SINGLE`` auf dem ListView, damit immer nur ein Benutzer in der Liste ausgewählt werden kann. 
* Verwenden Sie die Farbe ``android.R.color.holo_blue_light``als Hintergrundfarbe der gewählten Zeile ("Selector" des ListView). 

#### ``FloatingActionButton``zum Senden einer privaten Nachricht. 

* Geben Sie einen ``FloatingActionButton`` zum Layout der ``UserListActivity`` dazu.
* Positionieren Sie den Button im unteren rechten Bereich der Liste (auch, wenn sich die Display-Ausrichtung von Hochformat auf Querformat ändert). 
* Der Button soll "enabled" sein, falls ein Benutzer in der Liste ausgewählt ist, ansonsten "disabled" (nicht klickbar). 
* Wird der "enabled" Button geklickt, soll die ``CreateMessageActivity`` geöffnet werden. 
* Geben Sie dem Intent zum Öffnen der Activity den Usernamen des gewählten Benutzers als "Extra" mit. 

#### Anpassen der ``CreateMessageActivity`` und ``Message``

* Erweitern Sie die ``Message`` um ein optionales Feld/Property für den Empfänger einer Nachricht (siehe dazu auch die Beschreibung unter [http://messages.moarsoft.com/messages.html#post--rest-messages](http://messages.moarsoft.com/messages.html#post--rest-messages)). Zusätzlich implementieren Sie Getter und Setter für dieses neue Feld.
* Falls das Intent-Extra einen Usernamen enthält, verwenden Sie in der ``CreateMessageActivity`` diese Information, um beim Senden der Nachricht den Empfänger entsprechend zu setzen. 
* **Optional:** Erweitern Sie das Layout ``activity_create_message.xml``:
    *  um ein Label "Empfänger" und ein "disabled" Eingabe-Textfeld *über* dem Label und der Eingabe für den Betreff. 
    *  Zeigen Sie diese neuen Felder *nur* an, falls es sich um eine persönliche Nachricht für einen bestimmten Benutzer handelt - bei Nachrichten an alle Benutzer sind die Felder nicht sichtbar (Visibility). 
    *  Befüllen Sie das optionale Eingabe-Textfeld mit dem Vornamen und Nachnamen des Empfängers der privaten Nachricht. 

    
    
### Abgabe

Zippen Sie das fertiggestellte Projekt (den Android-Studio-Projekt-Folder mit allem, was dazugehört) und geben Sie dem zip-File den Namen "swa3t1.IHRNAME.zip" (beispielsweise "swa3t1.mair.zip"). 

Wenn möglich, bereiten Sie einen USB-Stick mit dem zip-File vor. Ansonsten bekommen Sie beim Prüfungstermin einen USB-Stick von mir, auf den Sie das File kopieren können.
        





 
