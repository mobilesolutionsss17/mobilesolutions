package android.campus02.at.assignment;

import java.util.ArrayList;
import java.util.List;

public class MessageStore {

    private MessageStore() {
    }

    private static MessageStore instance;

    private List<Message> messages = new ArrayList<>();

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public static MessageStore getInstance() {
        if (instance == null) {
            instance = new MessageStore();
        }
        return instance;
    }

}
