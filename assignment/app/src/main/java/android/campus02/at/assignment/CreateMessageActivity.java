package android.campus02.at.assignment;

import android.campus02.at.assignment.rest.MessageAPI;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateMessageActivity extends AppCompatActivity {

    private EditText subjectInput;
    private EditText contentInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_message);

        Button cancelButton = (Button) findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateMessageActivity.this.finish();
            }
        });

        subjectInput = (EditText) findViewById(R.id.subject_input);
        contentInput = (EditText) findViewById(R.id.content_input);

        Button sendButton = (Button) findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = subjectInput.getText().toString();
                String content = contentInput.getText().toString();
                SharedPreferences preferences =
                        PreferenceManager.getDefaultSharedPreferences(CreateMessageActivity.this);
                String senderUserName = preferences.getString("preference_username", null);
                if (senderUserName == null || senderUserName.isEmpty()) {
                    Toast.makeText(CreateMessageActivity.this, "Username missing!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Message newMessage = new Message(senderUserName, subject, content);
                sendMessage(newMessage);
            }
        });


    }

    private void sendMessage(Message message) {
        Call<Message> call = MessageAPI.getApi().send(message);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.code() == 200) {
                    finish();
                } else {
                    Toast.makeText(CreateMessageActivity.this,
                            "HTTP error: " + response.code(), Toast.LENGTH_SHORT).show();}
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Toast.makeText(CreateMessageActivity.this,
                        "Verbindungsfehler.", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
